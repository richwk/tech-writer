> Contact me on [LinkedIn](https://www.linkedin.com/in/richwalker1/)

# Richard Walker &ndash; Technical Writer

_Over 8 years experience in making complicated things simple and easy to understand._

This respository serves as a single, constantly up-to-date showcase of my technical writing experience and qualifications as well as my technical proficiency with GitLab, MadCap Flare, Markdown, and API documentation.<br><br>

## :briefcase: Portfolio

- [Sample User Manual](https://gitlab.com/richwk/tech-writer/-/blob/main/sample-user-manual.pdf) created in MadCap Flare (publicly available [here](https://customer-doc.cloud.gehealthcare.com/#/cdp/dashboard))
- [Sample API reference doc](https://gitlab.com/richwk/tech-writer/-/blob/main/sample-developer-api-reference-doc.md) written by developers
- [Sample API Document](https://gitlab.com/richwk/tech-writer/-/blob/main/sample-api-document.md) based on the reference doc<br><br>

## :black_nib: Technical Experience

**Senior Technical Writer** @ [GE Healthcare](https://www.linkedin.com/company/gehealthcare) - Freiburg, Germany _(May 2016 - October 2021)_ <br>
Medical software for all data management in the cardiovascular department.
  - Work with software developers, QA engineers, product owners, project managers, regulatory, and other SMEs in an agile environment to understand workflows and identify prioritization for documentation needs.
  - Write and revise technical documentation for software applications for various audiences with strict adherence to medical device standards.
  - Create and manage translation of user and security manuals in a single-source, structured XML authoring environment.
  - **_Tools/Technologies used:_** MadCap Flare, Git, Team Foundation Server, Confluence.<br><br>

**Technical Writer** @ [Transcend](https://www.linkedin.com/company/transcend-information-inc./) - Taipei, Taiwan _(September 2011 - April 2014)_ <br>
Manufacture of storage, multimedia and industrial products.
  - Create user manuals and quick start guides explaining how to use Transcend products safely and as intended.
  - Confirm that package design, package information, package inserts, and product datasheets are accurate and error-free.
  - **_Tools/Technologies used:_** Word, PowerPoint.<br><br>

## :mortar_board: Education

**Masters of Communication (Professional)** - Strong focus on research, critical thinking and problem solving.<br>
[Bond University](https://bond.edu.au/intl) - Gold Coast, Australia _(May 2014 - February 2016)_
- Dean’s List of Academic Excellence Award in September 2014
- Vice Chancellor’s List of Academic Excellence Award in January 2015 and May 2015
- Top Student BUSA Excellence Award in May 2015. <br><br>

**BSc Honours in Mathematics**<br>
[University of Strathclyde](https://www.strath.ac.uk/) - Glasgow, Scotland _(October 2000 - June 2004)_
